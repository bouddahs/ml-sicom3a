# Lab 2 statement

The objective of this lab is to illustrate discriminant analysis and naïve Bayes methods on both synthetic and computer vision (handwritten digits) datasets

_Note: For each notebook, read the cells and run the code, then follow the instructions/questions in the `Exercise` cells._

See the notebooks in the [4_discriminant_analysis](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/4_discriminant_analysis/) folder:

1. code your own linear discriminant analysis! Then compare with scikit-learn built-in methods, [`N1_LDA_code.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/4_discriminant_analysis/N1_LDA_code.ipynb)
2. apply (regularized) discriminant analysis to the zip codes handwritten digits, [`N2_discriminant-analysis_zip_digits.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/4_discriminant_analysis/N2_discriminant-analysis_zip_digits.ipynb)

The following notebooks are _optional_:

3. compare LDA and QDA decision boudaries on toy examples [`N3_plot_lda_qda.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/4_discriminant_analysis/N3_plot_lda_qda.ipynb)
4. illustrate the curse of dimensionality and the interest of regularization/shrinkage [`N4_curse_dimensionality_lda.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/4_discriminant_analysis/N4_curse_dimensionality_lda.ipynb)
