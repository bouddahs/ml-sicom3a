# Lab 7 statement

The objective of this lab is to illustrate Tree based classification and regression methods. Forest trees are introduced as a natural bagging method.  

_Note: For each notebook, read the cells and run the code, then follow the instructions/questions in the questions` or `Exercise` cells._

See the notebooks in the [`8_Trees_Boosting`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/8_Trees_Boosting/) folder


1. Firsts steps with classification trees [`N1_Classif_tree.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/8_Trees_Boosting/N1_Classif_tree.ipynb)


2. Examples of regression trees [`N2_a_Regression_tree.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/8_Trees_Boosting/N2_a_Regression_tree.ipynb) and cost complexity pruning methods [`N2_b_Cost_Complexity_Pruning_Regressor.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/8_Trees_Boosting/N2_b_Cost_Complexity_Pruning_Regressor.ipynb)


3. The next 3 notebooks llstrate the concept of bagging through the application of random forests. 
 [`N3_a_Random_Forest_Regression_tree.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/8_Trees_Boosting/N3_a_Random_Forest_Regression.ipynb) and [`N3_a_Random_Forest_Regression_tree.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/8_Trees_Boosting/N3_b_Random_Forest_Classif.ipynb)
  must be completed.  N3_c is optionnal, and describe some applications on real sonar data. 

4. Notebook ['N4_Trees_and_Boosting_iris_data_example.ipynb'](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/8_Trees_Boosting/N4_Trees_and_Boosting_iris_data_example.ipynb) is optionnal. It describes an examples of time sequential boosting algorithm: Adaboost. Application on Iris data set. 

