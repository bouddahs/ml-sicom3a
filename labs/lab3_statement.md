# Lab 3 statement

The objective of this lab is to illustrate regression models, in particular
stochastic gradient descent and ridge (L2) regularization.

_Note: For each notebook, read the cells and run the code, then follow the instructions/questions in the `Questions` or `Exercise` cells._

See the notebooks in the [5_linear_models_ridge](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/5_linear_models_ridge/) folder:


1. Experiment the learning rate parameter for stochastic gradient descent  [`N1_learning_rate_SGD.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/5_linear_models_ridge/N1_learning_rate_SGD.ipynb)
2. Experiment the effect of ridge (L2) regularization [`N2_L2_regularization.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/5_linear_models_ridge/N2_L2_regularization.ipynb)

3. Apply ridge (L2) regularization for the deconvolution problem [`N3_deconvolution_ridge.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/5_linear_models_ridge/N3_deconvolution_ridge.ipynb)
4. Perform ridge regression for predicting high dimensional NIR biscuits data  [`N4_ridge_NIR_biscuits.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/5_linear_models_ridge/N4_ridge_NIR_biscuits.ipynb)

*Note: The two first notebooks are simple interactive demonstrations on the tensorflow playground
and can be skipped for the class session*
